extends Control

var address = "http://127.0.0.1:46786/godot-telemetry"
var title_key = "b35db1097fd692a3dbd888ef2bb4c151"

var telemetry_data = {}

func _ready():
	$InputGameTitle.text = ProjectSettings.get_setting("application/config/name")
	
	_refresh()

func _on_ButtonPostRequest_button_down():
	_make_post_request(address, telemetry_data, false)

func _on_ButtonSetting_button_down():
	$WindowDialogSetting.popup_centered()
	
func _make_post_request(url, data_to_send, use_ssl):
	# Convert data to json string:
	var query = JSON.print(data_to_send)
	# Add 'Content-Type' header:
	var headers = ["Content-Type: application/json"]
	$HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	if response_code == 200 and result == $HTTPRequest.RESULT_SUCCESS:
		$LabelResponse.text = "SUCCEED"
	else:
		$LabelResponse.text = "FAILED"

func _update_telemetry_data():
	telemetry_data.title = $InputGameTitle.text
	telemetry_data.key = title_key
	telemetry_data.date = OS.get_datetime(true)
	telemetry_data.comment = $InputComment.text
	
func _update_json_preview():
	$RTLJSONPreview.text = JSON.print(telemetry_data, "	")

func _refresh():
	_update_telemetry_data()
	_update_json_preview()
	
func _on_Input_text_changed(new_text):
	_refresh()
	
func _on_WindowDialogSetting_about_to_show():
	$WindowDialogSetting/InputAddress.text = address
	$WindowDialogSetting/InputTitleKey.text = title_key

func _on_WindowDialogSetting_popup_hide():
	address = $WindowDialogSetting/InputAddress.text
	title_key = $WindowDialogSetting/InputTitleKey.text
