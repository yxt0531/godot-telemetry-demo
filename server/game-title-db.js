// add available game title here
const chalk = require("chalk");

var games = {
    "Godot Telemetry Demo": "b35db1097fd692a3dbd888ef2bb4c151",
};

const gameTitle = ["Godot Telemetry Demo"];

const isValidGame = (title, key) => {
    if (Object.keys(games).includes(title)) {
        if (games[title] == key) {
            console.log(chalk.green("game title is valid: ") + chalk.inverse(title) + " with key: " + chalk.yellow(key));
            return true;
        } else {
            console.log(chalk.red("invalid game key: ") + chalk.yellow(key));
            return false;
        }
    } else {
        console.log(chalk.red("invalid game title: ") + chalk.inverse(title));
        return false;
    }
};

module.exports = isValidGame;
