const chalk = require("chalk");

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const jsonParser = bodyParser.json(); // create application/json parser

const port = process.env.PORT || 46786;

// file system module to perform file operations
const fs = require("fs");

const isValidGame = require("./game-title-db");

// POST method route
app.post("/godot-telemetry", jsonParser, function (req, res) {
    console.log("\npost request received for game: " + chalk.inverse(req.body.title));

    if (isValidGame(req.body.title, req.body.key)) {
        let date_ob = new Date();

        var jsonContent = JSON.stringify(req.body);
        console.log(jsonContent);

        // generate file name and file path
        var file_path_root = "data";
        var file_path_title = req.body.title;
        var file_path_date = date_ob.getFullYear() + "-" + ("0" + (date_ob.getMonth() + 1)).slice(-2) + "-" + ("0" + date_ob.getDate()).slice(-2);

        var file_path = "./" + file_path_root + "/" + file_path_title + "/" + file_path_date + "/";
        var file_name = Date.now().toString() + ".json";
        // generate file name and file path end

        if (!fs.existsSync(file_path)) {
            // create directory if file path does does not exist
            console.log(chalk.red("file path directory does not exist"));
            fs.mkdirSync(file_path, { recursive: true });
            console.log("new directory created: " + file_path);
        }

        fs.writeFile(file_path + file_name, jsonContent, "utf8", function (err) {
            if (err) {
                console.log(chalk.red("unable to store data: " + file_path + file_name));
                console.log(chalk.red.inverse(err));
                res.status(400);
                res.send("rejected");
            } else {
                console.log(chalk.green("data stored to: " + file_path + file_name));
                res.status(200);
                res.send("accepted");
            }
        });
    } else {
        res.status(400);
        res.send("rejected");
    }
});

// start expressjs
app.listen(port, () => {
    console.log("godot telemetry server is running on port: " + port);
});
